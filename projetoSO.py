import threading
import time
sem = threading.Semaphore()

class Conta():
    def __init__(self,sal):
        self.saldo = sal
    # def retirada(self,valor):
    #     self.saldo = self.saldo - valor
    # def deposito(self,valor):
    #     self.saldo = self.saldo + valor
def transação1(valor):
    if conta1.saldo >= valor:
        sem.acquire()
        conta1.saldo = conta1.saldo - valor
        print("transferindo...\n")
        conta2.saldo = conta2.saldo + valor
        time.sleep(1)
        sem.release()
    return
def transação2(valor):
    if conta2.saldo >= valor:
        sem.acquire()
        conta2.saldo = conta2.saldo - valor
        print("transferindo...\n")
        conta1.saldo = conta1.saldo + valor
        time.sleep(1)
        sem.release()
    return

if __name__ == '__main__':
    start = time.perf_counter()
    conta1 = Conta(100)
    conta2 = Conta(100)
    valor = 100
    print("saldo conta1:", conta1.saldo)
    print("saldo conta2:", conta2.saldo)
    threads = []
    escolha = int(input("deseja transferir de qual conta?\n1 - conta1\n2 - conta2"))
    if escolha == 1:
        for i in range(0, 100, 1):
            t = threading.Thread(target=transação1(valor))
            t.start()
            threads.append(t)
        for thread in threads:
            thread.join()
    elif escolha == 2:
        for i in range(0, 100, 1):
            t = threading.Thread(target=transação2(valor))
            t.start()
            threads.append(t)
        for thread in threads:
            thread.join()
    else:
        print("ERRO - escolha uma conta!")


    print("\nTransferência completa.\n")
    # print("Saldo from:", conta1.saldo)
    # print("Saldo to:", conta2.saldo)
    # print("iniciando transação...\n")
    # t1 = threading.Thread(target= transação)
    # t2 = threading.Thread(target= transação)
    # t3 = threading.Thread(target= transação)
    # t4 = threading.Thread(target= transação)
    #
    # t1.start()
    # t2.start()
    # t3.start()
    # t4.start()
    #
    # t1.join()
    # t2.join()
    # t3.join()
    # t4.join()
    finish = time.perf_counter()
    print("tempo decorrido:", finish - start)
    print("\nSaldo From:", conta1.saldo)
    print("Saldo To:", conta2.saldo)
